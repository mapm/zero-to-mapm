From zero to mapm: for Linux
============================

Prerequisite knowledge
----------------------
Since you are on Linux, I assume a basic level of competence that I do not for
Windows and Mac users.

This is in part because I have to. One of the great things about Linux is how
much choice you have in it; your package manager could be apt, pacman,
portage, xbps, and more, and there are also alternative methods of installing
programs like snaps. As such, I cannot just say "run pacman" the same way I
can say "run brew" for Mac OS. Therefore, I will instead say "Install Rust
through your package manager."

You should know what a terminal is, how to execute commands in one, and the
basic commands (ls, cd, mkdir, etc).

You also have to know what a LaTeX document looks like. (Typically, experience
with an online editor like Overleaf gives you a small subset of this
knowledge; it should be sufficient to start out with.) For a LaTeX primer,
peruse

	https://www.dennisc.net/writing/tech/latex

It is helpful, though not mandatory, for you to have some knowledge in config
files, particularly for the shell you use. That way you will not be blindly following
instructions and can fix your own problems or recognize nuances when they
appear. The same goes for symlinks.

Most importantly, you should be able to use Google to solve your own problems.

Dependencies
------------
Install Git through your package manager
Install TeX Live from source (https://www.tug.org)
	You may also use your package manager, but I recommend against this.
	(Unless your package manager is portage.)
Install rustup through your package manager
	Install Rust nightly edition through rustup

TeX Setup
---------
Prerequisites: TeX Live

Hopefully you have already read the LaTeX primer I linked in the "Prerequisite
knowledge" section. You may even want to read

	https://www.dennisc.net/writing/tech/tex-overview

for a higher-level overview of how TeX behaves on your system.

In any case, here is a short summary of how TEXMF works:
when you call \documentclass{article}, LaTeX looks through your TEXMF
directories. Since article is a standard document class, it will be installed
in TEXMFDIST, where all of your pre-installed LaTeX contents are.

Some mapm templates are associated with a custom TeX class. As an example,
Math Advance's template for its flagship contest, the MAT, has one. You can
see for yourself at

	https://gitlab.com/mathadvance/tex/mat

The mapm template is stored in the mapm/ subdirectory of this repository. If
you are making your own mapm template that has a custom document class, I
strongly recommend you do the same.

In the instructions that follow, any words in SCREAMING_SNAKE_CASE should be
replaced with the appropriate value for your particular template.

If the template you want to use has a custom document class associated with
it, you should run

	$ git clone URL_TO_TEMPLATE ~/texmf/tex/latex/CLASS_NAME

I strongly recommend that you symlink the mapm template to the template
directory, just because you can update the template and the document class in
one fell swoop by pulling the repository. Do this through

	$ ln -s ~/texmf/tex/latex/CLASS_NAME/mapm ~/.config/mapm/templates/TEMPLATE_NAME

This requires that the mapm/templates/ parent directory already be made.
mapm-cli should handle this for you, so first run

	$ mapm

and set a profile in order to initialize all of mapm's config directories.

Mapm Setup
----------
Prerequisites: Rust

I compile mapm-cli with the nightly edition of Rust. Some times you may not
need nightly to install mapm-cli properly, but I would switch to nightly just
to be safe. In order to do that, run

	$ rustup default nightly

This will install the nightly toolchain for you if necessary.

To install mapm, run

	$ cargo install mapm-cli

Please read the documentation of mapm at

	https://mapm.mathadvance.org/

before you proceed any further; this way you will be familiar with the
command-line arguments, as well as how template and contest files should work.
This document will work through examples of problem, template, and contest
files.

It is important to note that, though the package is called mapm-cli, the
binary is named mapm.

A set of examples, with no commentary, can be found at

	https://gitlab.com/mapm/lib/-/tree/master/examples

and the real-world mapm sources for Math Advance's contests, along with
a script for fetching contests into your own mapm system, can be found at

	https://gitlab.com/mathadvance/contest-releases

To initialize mapm, just run
	
	$ mapm

and it will prompt you to set a default profile.

In order to create a template, go inside

	~/.config/mapm/templates

and make a subdirectory with your template name. Instructions for what should
go inside a mapm template can be found in the documentation, at

	https://mapm.mathadvance.org/contests/template/

Instructions for how to write and compile a contest file are at

	https://mapm.mathadvance.org/contests/

Configuring an editor for mapm edit
-----------------------------------
The default editor is nano. You can change this.

mapm will respect the EDITOR environment variable. In order to set it, run
	
	$ vim ~/.bashrc

if using Bash, and

	$ vim ~/.zshenv

if using Zsh.

Unless you have done this before, it will make a new file; this behavior is
expected.

Inside the file, add

	export EDITOR=vim

You may replace vim with any editor of your choice; my friends like to use
VSCode, and I personally recommend Nano if you do not know how to use Vim.
